<!DOCTYPE html>

<html lang="en-US" class="default-page">
    <head>
		<title>The Gals Cleaning Service | Quote Request</title>	
        <link rel="canonical" href="https://www.thegalscleaningservice.com/quote-request/">
        <meta name="description" content="At The Gals Cleaning Services, we deliver personalized commercial cleaning services to office buildings, medical and industrial facilties. Get a free consultation over the phone by filling this form.">

        <?php include('partials/head.php'); ?>


        <meta property="fb:app_id" content=""/>
        <meta property="og:locale" content="en_US">
        <meta property="og:type" content="website">
        <meta property="og:title" content="Quote Request | The Gals Cleaning Services">
        <meta property="og:description" content="At The Gals Cleaning Services, we deliver personalized commercial cleaning services to office buildings, medical and industrial facilties. Get a free consultation over the phone by filling this form.">
        <meta property="og:url" content="https://www.thegalscleaningservice.com/quote-request/">
        <meta property="og:site_name" content="The Gals Cleaning Services - Quote Request">
        <meta property="og:image" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png" />
        <meta property="og:image:secure_url" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png">

        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="https://www.thegalscleaningservice.com/quote-request/">
        <meta name="twitter:description" content="At The Gals Cleaning Services, we deliver personalized commercial cleaning services to office buildings, medical and industrial facilties. Get a free consultation over the phone by filling this form.">
        <meta name="twitter:title" content="Quote Request | The Gals Cleaning Services">
        <meta name="twitter:image" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png">

        <script type="application/ld+json">{"@context":"https://schema.org","@type":"WebSite","@id":"https://www.thegalscleaningservice.com/quote-request/#website","url":"https://www.thegalscleaningservice.com/quote-request/","name":"The Gals Cleaning Services | Quote Request","potentialAction":{"@type":"SearchAction","target":"https://www.thegalscleaningservice.com/quote-request/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
        <script>
            $(document).ready(function() {

				var request;

              	$("#quoteForm").submit(function(event){
	                event.preventDefault();

					// Abort any pending request
				    if (request) {
				        request.abort();
				    }

					// setup some local variables
    				var $form = $(this);

					// Let's select and cache all the fields
    				var $inputs = $form.find("input, select, button, textarea");

					// Serialize the data in the form
					var serializedData = $form.serialize();

					// Let's disable the inputs for the duration of the Ajax request.
					// Note: we disable elements AFTER the form data has been serialized.
					// Disabled form elements will not be serialized.
					// $inputs.prop("disabled", true);

	                request = $.ajax({
        				url: "quoteMail",
	                  	type: "post",
	                  	data: serializedData
	                });

					// Callback handler that will be called on success
				    request.done(function (response, textStatus, jqXHR){

					    if (response.status == 0) {
					        // Success! Do something successful, like show success modal
                            jQuery.noConflict();
					        $('#successEmail').modal('show');
					    } else {
					        // Oh no, failure - notify the user
                            jQuery.noConflict();
                            $('#failEmail').modal('show');
					    }

                        $( "#failBtn" ).click(function() {
                            jQuery.noConflict();
                            $('#failEmail').modal('hide');
                        });

                        $( "#passBtn" ).click(function() {
                            window.location.reload();
                        });

				    });

				    // Callback handler that will be called on failure
				    request.fail(function (jqXHR, textStatus, errorThrown){
						// Say you have a <div class="message"></div> somewhere, maybe under your form
    					//$('.message').html('Error: ' + textStatus + ', ' + errorThrown)
				    });

					// Callback handler that will be called regardless
				    // if the request failed or succeeded
				    request.always(function () {
				        // Reenable the inputs
				        $inputs.prop("disabled", false);
				    });
              	});
        	});
      	</script>

    </head>

    <body class="has-hero-intent has-mobi-footer index-template" data-gr-c-s-loaded="true" cz-shortcut-listen="true">        
        <?php include('partials/header.php'); ?>
        
        <div class="site-content">            
            <main>              
                <section class="page-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="page-header">
                                    <h1>Your Cleaning Quote</h1>
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <h2>Let’s Get You Cleaned Up</h2>
                                <p>Use this form to receive a free quote from us with no obligations. You may also use this form to give us a heads up about the nature of the project, any challenges or special considerations you may want us to know and any complexity you may think is there. 
                                <br/><br/>
                                Please fill out as many fields as possible, specially all required fields. This will allows to communicate with you as effectively as possible. Don’t worry, we will not bother you and we never share any information without your consent. </p>  
                            </div>
                        </div>
                    </div>
                </section>    
                <section class="black-bg align-form"> 
                    <div class="container">   
                        <form id="quoteForm" method="POST" autocomplete="off">
                            <div class="row form-container">
                                <div class="col-xs-12 col-md-3">
                                    <div class="form-group">
                                        <label for="zip">Zip Code</label>
                                        <input type="text" class="form-control" id="zip" name="zip" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-3">    
                                    <div class="form-group">
                                        <label for="facility">Facility Name</label>
                                        <input type="text" class="form-control" id="facility" name="facility" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-3">    
                                    <div class="form-group">
                                        <label for="interested">Interested In</label>
                                        <select class="form-control" id="interested" name="interested">
                                            <option value=""></option>
                                            <option value="Commercial Cleaning">Commercial Cleaning</option>
                                            <option value="Medical Room Sanitation">Medical Room Sanitation</option>
                                            <option value="Industrial Cleaning">Industrial Cleaning</option>
                                            <option value="Floor &amp; Carpeting Cleaning">Floor &amp; Carpeting Cleaning</option>
                                            <option value="Window Cleaning Services">Window Cleaning Services</option>
                                            <option value="School &amp; Child Care Cleaning">School &amp; Child Care Cleaning</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-3">    
                                    <div class="form-group">
                                        <label for="frequency">Frequency of Service</label>
                                        <select class="form-control" id="frequency" name="frequency">
                                                <option value=""></option>
                                                <option value="Daily">Daily</option>
                                                <option value="6X Per Week">6X Per Week</option>
                                                <option value="5X Per Week">5X Per Week</option>
                                                <option value="4X Per Week">4X Per Week</option>
                                                <option value="3X Per Week">3X Per Week</option>
                                                <option value="2X Per Week">2X Per Week</option>
                                                <option value="Once a Week">Once a Week</option>
                                                <option value="One-Time Service">One-Time Service</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row form-container">
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group">
                                        <label for="name">Your Name</label>
                                        <input type="text" class="form-control" id="name" name="name" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4">    
                                    <div class="form-group">
                                        <label for="name">Your Email</label>
                                        <input type="email" class="form-control" id="email" name="email" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4"> 
                                    <div class="form-group">
                                        <label for="phone">Phone Number</label>
                                        <input type="text" class="form-control" id="phone" name="phone" />
                                    </div>                                   
                                </div>
                            </div>
                            <div class="row form-container">
                                <div class="col-xs-6 col-md-8">
                                    <img id="captcha" src="/securimage/securimage_show.php" alt="CAPTCHA Image" />
                                    <br/>
                                    <input type="text" name="captcha_code" size="10" maxlength="6" autocomplete="off" />
                                    <br/>
                                    <a href="#" id="secureText" onclick="document.getElementById('captcha').src = '/securimage/securimage_show.php?' + Math.random(); return false">
                                        <small class="refresh-captcha">Refresh Image</small>
                                    </a>
                                </div>
                                <div class="col-xs-6 col-md-4 alignend">
                                    <button type="submit" class="btn btn-brand-alt" id="btnSubmit">Submit</button>  
                                </div>
                            </div> 
                        </form>
                    </div>
                </section>
            </main>
        </div>

        <div id="successEmail" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body text-center">
                        <i class="fas fa-thumbs-up" style="color: #4caf50; border-radius: 100%; border: 5px solid #4caf50; padding: 20px; margin-bottom: 25px; font-size: 50px;">
                        </i>
                        <p style="color: #4caf50;font-weight: 500;"> Thank you for your message! Someone will contact you very soon.</p>
                    </div>
                    <div class="modal-footer text-center">
                        <button id="passBtn" type="button" class="btn btn-success btn-block" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="failEmail" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body text-center">
                        <i class="fas fa-exclamation-triangle" style="color: #dc3545; border-radius: 100%; border: 5px solid #dc3545;  padding: 20px; margin-bottom: 25px; font-size: 50px;"></i>
                        <p style="color: #dc3545;font-weight: 500;">Something went wrong. Review your form and make sure nothing was missed.</p>
                    </div>
                    <div class="modal-footer text-center">
                        <button id="failBtn" type="button" class="btn btn-danger btn-block" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        
        <?php include('partials/footer.php'); ?>

        <script type="text/javascript" src="/assets/js/sensei-foot-libs.min.js"></script>
        <script type="text/javascript" src="/assets/js/sensei-bentobox.min.js"></script>
          
    </body>
	
</html>