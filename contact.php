<!DOCTYPE html>

<html lang="en-US" class="default-page">
    <head>
		<title>The Gals Cleaning Service | Contact</title>	
        <link rel="canonical" href="https://www.thegalscleaningservice.com/contact/">
        <meta name="description" content="Interested in The Gals Cleaning Services personalized commercial cleaning services? Contact us today for a complimentary estimate for your office or building.">

        <?php include('partials/head.php'); ?>

        <meta property="fb:app_id" content=""/>
        <meta property="og:locale" content="en_US">
        <meta property="og:type" content="website">
        <meta property="og:title" content="Contact | The Gals Cleaning Services">
        <meta property="og:description" content="Interested in The Gals Cleaning Services personalized commercial cleaning services? Contact us today for a complimentary estimate for your office or building.">
        <meta property="og:url" content="https://www.thegalscleaningservice.com/contact/">
        <meta property="og:site_name" content="The Gals Cleaning Services - Contact">
        <meta property="og:image" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png" />
        <meta property="og:image:secure_url" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png">

        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="https://www.thegalscleaningservice.com/contact/">
        <meta name="twitter:description" content="Interested in The Gals Cleaning Services personalized commercial cleaning services? Contact us today for a complimentary estimate for your office or building.">
        <meta name="twitter:title" content="Contact | The Gals Cleaning Services">
        <meta name="twitter:image" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png">

        <script type="application/ld+json">{"@context":"https://schema.org","@type":"WebSite","@id":"https://www.thegalscleaningservice.com/contact/#website","url":"https://www.thegalscleaningservice.com/contact/","name":"The Gals Cleaning Services | Contact","potentialAction":{"@type":"SearchAction","target":"https://www.thegalscleaningservice.com/contact/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>

        <script>
            $(document).ready(function() {

				var request;

              	$("#contactForm").submit(function(event){
	                event.preventDefault();

					// Abort any pending request
				    if (request) {
				        request.abort();
				    }

					// setup some local variables
    				var $form = $(this);

					// Let's select and cache all the fields
    				var $inputs = $form.find("input, select, button, textarea");

					// Serialize the data in the form
					var serializedData = $form.serialize();

					// Let's disable the inputs for the duration of the Ajax request.
					// Note: we disable elements AFTER the form data has been serialized.
					// Disabled form elements will not be serialized.
					// $inputs.prop("disabled", true);

	                request = $.ajax({
        				url: "contactMail",
	                  	type: "post",
	                  	data: serializedData
	                });

					// Callback handler that will be called on success
				    request.done(function (response, textStatus, jqXHR){

					    if (response.status == 0) {
					        // Success! Do something successful, like show success modal
                            jQuery.noConflict();
					        $('#successEmail').modal('show');
					    } else {
					        // Oh no, failure - notify the user
                            jQuery.noConflict();
                            $('#failEmail').modal('show');
					    }

                        $( "#failBtn" ).click(function() {
                            jQuery.noConflict();
                            $('#failEmail').modal('hide');
                        });

                        $( "#passBtn" ).click(function() {
                            window.location.reload();
                        });

				    });

				    // Callback handler that will be called on failure
				    request.fail(function (jqXHR, textStatus, errorThrown){
						// Say you have a <div class="message"></div> somewhere, maybe under your form
    					//$('.message').html('Error: ' + textStatus + ', ' + errorThrown)
				    });

					// Callback handler that will be called regardless
				    // if the request failed or succeeded
				    request.always(function () {
				        // Reenable the inputs
				        $inputs.prop("disabled", false);
				    });
              	});
        	});
      	</script>

    </head>

    <body class="has-hero-intent has-mobi-footer index-template" data-gr-c-s-loaded="true" cz-shortcut-listen="true">        
        <?php include('partials/header.php'); ?>
        
        <div class="site-content">            
            <main>              
                <section class="page-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="page-header">
                                    <h1>Contact Us</h1>
                                </div> 
                            </div>
                        </div>
                    </div>
                </section>    
                <section class="black-bg align-form" style="min-height: 807px;"> 
                    <div class="container">   
                        <form id="contactForm" method="POST"  autocomplete="off">
                            <div class="row form-container alignstart">
                                <div class="col-xs-12 col-md-8">
                                    <div class="form-group">
                                        <label for="name">Your Name</label>
                                        <input type="text" class="form-control" id="name" name="name"/>
                                    </div>  
                                    <div class="form-group">
                                        <label for="name">Your Email</label>
                                        <input type="email" class="form-control" id="email" name="email" />
                                    </div>
                                    <div class="form-group">
                                        <label for="phone">Phone Number</label>
                                        <input type="text" class="form-control" id="phone" name="phone" />
                                    </div>    
                                    <div class="form-group">
                                        <label for="comments">Comments</label>
                                        <textarea type="text" class="form-control" id="comments" name="comments"></textarea>
                                    </div> 
                                    <div class="row">
                                        <div class="col-xs-6 col-md-8">
                                            <img id="captcha" src="/securimage/securimage_show.php" alt="CAPTCHA Image" />
                                            <br/>
                                            <input type="text" name="captcha_code" size="10" maxlength="6" autocomplete="off" />
                                            <br/>
                                            <a href="#" id="secureText" onclick="document.getElementById('captcha').src = '/securimage/securimage_show.php?' + Math.random(); return false">
                                                <small class="refresh-captcha">Refresh Image</small>
                                            </a>
                                        </div>
                                        <div class="col-xs-6 col-md-4 alignend">
                                            <button type="submit" class="btn btn-brand-alt" id="btnSubmit">Send</button>  
                                        </div>
                                    </div>                     
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="contact-info">
                                        <h3>Contact Info</h3>
                                        <ul class="fa-ul unstyled text-left">
                                            <li><i class="fas fa-phone"></i> <a href="tel:+7176459633" class="tel">(717) 645-9633</a></li>
                                            <li><i class="fas fa-envelope"></i> <a href="mailto:thegalscleaningservice@yahoo.com">thegalscleaningservice@yahoo.com</a></li>
                                            <li><i class="fas fa-map-marker-alt"></i> <span>112 E. Columbia Rd. Enola, PA 17025</span></li> 
                                            <li><i class="fab fa-facebook-square"></i> <a href="https://www.facebook.com/TheGalsCleaningService/" target="_blank">Like Our Page!</a></li>
                                            <li><i class="fab fa-twitter-square"></i> <a href="https://twitter.com/TheGalsCleaning" target="_blank">Follow Us!</a></li>
                                        </ul> 
                                    </div>
                                    <div class="hours">
                                        <h3>Hours of Operation</h3>
                                        <p class="">We are open for service <strong>24 hrs – 365 days</strong>. Emergency after hour services provided.</p>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </main>
        </div>


        <div id="successEmail" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body text-center">
                        <i class="fas fa-thumbs-up" style="color: #4caf50; border-radius: 100%; border: 5px solid #4caf50; padding: 20px; margin-bottom: 25px; font-size: 50px;">
                        </i>
                        <p style="color: #4caf50;font-weight: 500;"> Thank you for your message! Someone will contact you very soon.</p>
                    </div>
                    <div class="modal-footer text-center">
                        <button id="passBtn" type="button" class="btn btn-success btn-block" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="failEmail" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body text-center">
                        <i class="fas fa-exclamation-triangle" style="color: #dc3545; border-radius: 100%; border: 5px solid #dc3545;  padding: 20px; margin-bottom: 25px; font-size: 50px;"></i>
                        <p style="color: #dc3545;font-weight: 500;">Something went wrong. Review your form and make sure nothing was missed.</p>
                    </div>
                    <div class="modal-footer text-center">
                        <button id="failBtn" type="button" class="btn btn-danger btn-block" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        
        <?php include('partials/footer.php'); ?>

        <script type="text/javascript" src="/assets/js/sensei-foot-libs.min.js"></script>
        <script type="text/javascript" src="/assets/js/sensei-bentobox.min.js"></script>
        
    </body>
	
</html>