<!DOCTYPE html>

<html lang="en-US" class="default-page">
    <head>
		<title>The Gals Cleaning Service | School &amp; Child Care Cleaning</title>	
        <link rel="canonical" href="https://www.thegalscleaningservice.com/school-child-care-cleaning/">
        <meta name="description" content="At The Gals Cleaning Services, we deliver personalized school &amp; child care cleaning services to office buildings, medical and industrial facilties. We take pride in what we do and ensure nothing short of maximum results. Call today to schedule a free consultation.">

        <?php include('partials/head.php'); ?>


        <meta property="fb:app_id" content=""/>
        <meta property="og:locale" content="en_US">
        <meta property="og:type" content="website">
        <meta property="og:title" content="School &amp; Child Care Cleaning | The Gals Cleaning Services">
        <meta property="og:description" content="At The Gals Cleaning Services, we deliver personalized school &amp; child care cleaning services to office buildings, medical and industrial facilties. We take pride in what we do and ensure nothing short of maximum results. Call today to schedule a free consultation.">
        <meta property="og:url" content="https://www.thegalscleaningservice.com/school-child-care-cleaning/">
        <meta property="og:site_name" content="The Gals Cleaning Services - School &amp; Child Care Cleaning">
        <meta property="og:image" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png" />
        <meta property="og:image:secure_url" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png">

        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="https://www.thegalscleaningservice.com/school-child-care-cleaning/">
        <meta name="twitter:description" content="At The Gals Cleaning Services, we deliver personalized school &amp; child care cleaning services to office buildings, medical and industrial facilties. We take pride in what we do and ensure nothing short of maximum results. Call today to schedule a free consultation.">
        <meta name="twitter:title" content="School &amp; Child Care Cleaning | The Gals Cleaning Services">
        <meta name="twitter:image" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png">

        <script type="application/ld+json">{"@context":"https://schema.org","@type":"WebSite","@id":"https://www.thegalscleaningservice.com/school-child-care-cleaning/#website","url":"https://www.thegalscleaningservice.com/school-child-care-cleaning/","name":"The Gals Cleaning Services | School &amp; Child Care Cleaning","potentialAction":{"@type":"SearchAction","target":"https://www.thegalscleaningservice.com/school-child-care-cleaning/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
    </head>

    <body class="has-hero-intent has-mobi-footer index-template" data-gr-c-s-loaded="true" cz-shortcut-listen="true">        
        <?php include('partials/header.php'); ?>
        
        <div class="site-content">            
            <main>              
                <section class="page-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="page-header">
                                    <h1>School &amp; Child Care Cleaning</h1>
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <h2>Because We Care About Our Kids Health</h2>
                                <p>Children, while developing, become vulnerable to germs. It is a common problem that all parents are accustomed to. However, it is always safer to reduce the frequency and severity of these outbreaks. The Gals Cleaning Service goes above and beyond to ensure all surfaces and areas are thoroughly cleaned and sanitized. This keeps kids and teachers safer and keeps them coming back the next day.</p>  
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <p>Child care facilities and infant care facilities require a especial consideration due to feces and vomit being more frequent as it is normal for an infant to have these accidents. Our staff ensures that all areas are sanitized at the end of the day even if your staff ever so diligently attended to it immediately. When it comes to our kids, we can never be too careful.  
                                <br/> <br/>
                                Read more about the Laws and Regulations Governing the Control of Communicable Diseases to familiarize yourself with all that is being governed.</p>   
                                <div class="text-center">   
                                    <a href="http://www.statutes.legis.state.tx.us/Docs/HS/htm/HS.341.htm" target="_blank" class="btn btn-primary">See it Here</a>
                                </div> 
                            </div>
                        
                            <div class="col-xs-12 col-md-6 text-center">
                                <?php include('partials/call-out-box.php'); ?>
                            </div>
                        </div>
                    </div>
                </section>
            </main>
        </div>
        
        <?php include('partials/footer.php'); ?>

        <script type="text/javascript" src="/assets/js/sensei-foot-libs.min.js"></script>
        <script type="text/javascript" src="/assets/js/sensei-bentobox.min.js"></script>
        
    </body>
	
</html>