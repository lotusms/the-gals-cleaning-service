<!DOCTYPE html>

<html lang="en-US" class="default-page">
    <head>
		<title>The Gals Cleaning Service | Privacy Policy</title>	
        <link rel="canonical" href="https://www.thegalscleaningservice.com/privacy-policy/">
        <meta name="description" content="The Gals Cleaning Services privacy policy. Call today to schedule a free consultation.">

        <?php include('partials/head.php'); ?>


        <meta property="fb:app_id" content=""/>
        <meta property="og:locale" content="en_US">
        <meta property="og:type" content="website">
        <meta property="og:title" content="Privacy Policy | The Gals Cleaning Services">
        <meta property="og:description" content="The Gals Cleaning Services privacy policy. Call today to schedule a free consultation.">
        <meta property="og:url" content="https://www.thegalscleaningservice.com/privacy-policy/">
        <meta property="og:site_name" content="The Gals Cleaning Services - Privacy Policy">
        <meta property="og:image" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png" />
        <meta property="og:image:secure_url" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png">

        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="https://www.thegalscleaningservice.com/privacy-policy/">
        <meta name="twitter:description" content="The Gals Cleaning Services privacy policy. Call today to schedule a free consultation.">
        <meta name="twitter:title" content="Privacy Policy | The Gals Cleaning Services">
        <meta name="twitter:image" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png">

        <script type="application/ld+json">{"@context":"https://schema.org","@type":"WebSite","@id":"https://www.thegalscleaningservice.com/privacy-policy/#website","url":"https://www.thegalscleaningservice.com/privacy-policy/","name":"The Gals Cleaning Services | Privacy Policy","potentialAction":{"@type":"SearchAction","target":"https://www.thegalscleaningservice.com/privacy-policy/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
    </head>

    <body class="has-hero-intent has-mobi-footer index-template" data-gr-c-s-loaded="true" cz-shortcut-listen="true">        
        <?php include('partials/header.php'); ?>
        
        <div class="site-content">            
            <main>              
                <section class="page-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="page-header">
                                    <h1>Privacy Policy</h1>
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12" style="padding-bottom: 80px;">
                                <h4>What information do we collect?</h4>

                                <p>
                                    We collect information from you when you respond to a survey.
                                    <br><br>
                                    When ordering or registering on our site, as appropriate, you may be asked to enter your name, e-mail address, phone number or name. You may, however, visit our site anonymously.
                                </p>
                                
                                <h4>What do we use your information for?</h4>
                                <p>Any of the information we collect from you may be used in one of the following ways:</p>
                                <ul class="list-indent">
                                    <li>To personalize your experience – (your information helps us to better respond to your individual needs)</li>
                                    <li>To process transactions</li>
                                </ul>
                                
                                <p>
                                    Your information, whether public or private, will not be sold, exchanged, transferred, or given to any other company for any reason whatsoever, without your consent, other than for the express purpose of delivering the purchased product or service requested.
                                </p>
                                
                                <h4>How do we protect your information?</h4>
                                <p>
                                    We implement a variety of security measures to maintain the safety of your personal information when you place an order or enter, submit, or access your personal information.
                                    <br><br>
                                    We offer the use of a secure server. All supplied sensitive/credit information is transmitted via Secure Socket Layer (SSL) technology and then encrypted into our Payment gateway providers database only to be accessible by those authorized with special access rights to such systems, and are required to keep the information confidential.
                                    <br><br>
                                    After a transaction, your private information (credit cards, social security numbers, financials, etc.) will not be stored on our servers.
                                </p>
                                
                                <h4>Do we use cookies?</h4>
                                <p>We do not use cookies.</p>
                                
                                <h4>Do we disclose any information to outside parties?</h4>
                                <p>We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information. This does not include trusted third parties who assist us in operating our website, conducting our business, or servicing you, so long as those parties agree to keep this information confidential. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect ours or others rights, property, or safety. However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.</p>
                                
                                <h4>Third party links</h4>
                                <p>Occasionally, at our discretion, we may include or offer third party products or services on our website. These third party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.</p>
                                
                                <h4>California Online Privacy Protection Act Compliance</h4>
                                <p>
                                    Because we value your privacy we have taken the necessary precautions to be in compliance with the California Online Privacy Protection Act. We therefore will not distribute your personal information to outside parties without your consent.
                                    <br><br>
                                    As part of the California Online Privacy Protection Act, all users of our site may make any changes to their information at any time by logging into their control panel and going to the ‘Edit Profile’ page.
                                </p>
                                
                                <h4>Childrens Online Privacy Protection Act Compliance</h4>
                                <p>We are in compliance with the requirements of COPPA (Childrens Online Privacy Protection Act), we do not collect any information from anyone under 13 years of age. Our website, products and services are all directed to people who are at least 13 years old or older.</p>
                                
                                <h4>Online Privacy Policy Only</h4>
                                <p>This online privacy policy applies only to information collected through our website and not to information collected offline.</p>
                                
                                
                                <h4>Terms and Conditions</h4>
                                <p>Please also visit our Terms and Conditions section establishing the use, disclaimers, and limitations of liability governing the use of our website at Terms of Use</p>
                                
                                <h4>Your Consent</h4>
                                <p>By using our site, you consent to our web site privacy policy.</p>
                                
                                <h4>Changes to our Privacy Policy</h4>
                                <p>If we decide to change our privacy policy, we will post those changes on this page. This policy was last modified on 01/10/2019</p>
                                
                                <h4>Contacting Us</h4>
                                <p>If there are any questions regarding this privacy policy you may contact us using the information below.</p>
                                
                                <span><a href="http://www.thegalscleaningservice.com/">www.thegalscleaningservice.com</a></span><br>
                                <span>112 E. Columbia Rd. Enola, PA 17025</span><br>
                                <span><a href="mailto:thegalscleaningservice@yahoo.com">thegalscleaningservice@yahoo.com</a></span><br>
                                <span>Phone: (717) 645-9633</span><br>
                            </div>
                        </div>
                    </div>
                </section>    
            </main>
        </div>
        
        <?php include('partials/footer.php'); ?>

        <script type="text/javascript" src="/assets/js/sensei-foot-libs.min.js"></script>
        <script type="text/javascript" src="/assets/js/sensei-bentobox.min.js"></script>
        
    </body>
	
</html>