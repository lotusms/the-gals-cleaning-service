<!DOCTYPE html>

<html lang="en-US" class="default-page">
    <head>
		<title>The Gals Cleaning Service | About</title>	
        <link rel="canonical" href="https://www.thegalscleaningservice.com/about/">
        <meta name="description" content="At The Gals Cleaning Services, we deliver personalized commercial cleaning services to office buildings, medical and industrial facilties. We take pride in what we do and ensure nothing short of maximum results. Call today to schedule a free consultation.">

        <?php include('partials/head.php'); ?>

        <meta property="fb:app_id" content=""/>
        <meta property="og:locale" content="en_US">
        <meta property="og:type" content="website">
        <meta property="og:title" content="About Our Commercial Cleaning Services | The Gals Cleaning Services">
        <meta property="og:description" content="At The Gals Cleaning Services, we deliver personalized commercial cleaning services to office buildings, medical and industrial facilties. We take pride in what we do and ensure nothing short of maximum results. Call today to schedule a free consultation.">
        <meta property="og:url" content="https://www.thegalscleaningservice.com/about/">
        <meta property="og:site_name" content="The Gals Cleaning Services - About">
        <meta property="og:image" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png" />
        <meta property="og:image:secure_url" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png">

        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="https://www.thegalscleaningservice.com/about/">
        <meta name="twitter:description" content="At The Gals Cleaning Services, we deliver personalized commercial cleaning services to office buildings, medical and industrial facilties. We take pride in what we do and ensure nothing short of maximum results. Call today to schedule a free consultation.">
        <meta name="twitter:title" content="About | The Gals Cleaning Services">
        <meta name="twitter:image" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png">

        <script type="application/ld+json">{"@context":"https://schema.org","@type":"WebSite","@id":"https://www.thegalscleaningservice.com/about/#website","url":"https://www.thegalscleaningservice.com/about/","name":"The Gals Cleaning Services | About","potentialAction":{"@type":"SearchAction","target":"https://www.thegalscleaningservice.com/about/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>


        <link rel="stylesheet" type="text/css" href="/assets/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick-theme.css"/>

    </head>

    <body class="has-hero-intent has-mobi-footer index-template" data-gr-c-s-loaded="true" cz-shortcut-listen="true">        
        <?php include('partials/header.php'); ?>
        
        <div class="site-content">            
            <main>              
                <section class="page-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="page-header">
                                    <h1>About The Gals</h1>
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p>The Gals Cleaning Service is a family owned and operated business that has been providing outstanding, high-quality, experienced, and professional janitorial services for the greater Harrisburg area since 2005. 
                                <br/><br/>
                                When you hire us for your commercial janitorial needs, the same individual or team is there each and every time we provide a service. As a result, a relationship is built and we can’t value that enough. We are insured, bonded and we are cleared of the Megan’s Law check. We are proud to say our family comes from a drug free non-smoking background. All of our employees, including the owner, will always require to wear an ID badge that states their name and contact information at all buildings, client’s properties, and job sites. This will ensure security and safety in all workplaces.
                                <br/><br/>
                                We are qualified to perform medical and school janitorial services that follow a specific code of sterilization and sanitation. In our growing history, we have accumulated the experience and technology that are sure to fit a wide range of cleaning services. </p>  
                            </div>
                        </div>
                    </div>
                </section>    
                <section class="black-bg"> 
                    <div class="container">   
                        <div class="row about-us">
                            <div class="col-xs-12 col-sm-4 text-center">                    
                                <img src="/assets/img/about/Angela2.png" alt="Angela Martin" class="text-center round-img">
                                <h4>Angela Martin</h4>
                                <p class="text-justify">Angela Martin is the owner of the gals cleaning service. She has been providing high quality janitorial cleaning services for over 10 years.  She enjoys the outdoors, cooking and spending time with her husband tom and English bulldog Harper blue.  She resides in Harrisburg, PA</p>
                            </div>
                            <div class="col-xs-12 col-sm-4 text-center">
                                <img src="/assets/img/about/vision2.png" alt="vision statement" class="text-center round-img">
                                <h4>Our Vision</h4>
                                <p class="text-justify">Our mission is to provide a strong relationship with our clients for life.  The standards we have when it comes to janitorial cleaning services are much higher then our competitors.  This is true because we still have been serving all of our current clients for years.
                                <br/><br/>
                                Look no further for an honest, consistant, high standard janitorial cleaning service.  The Gals Cleaning Service is the ultimate janitorial cleaner.  You are going to love what The Gals Cleaning Service can do for your valued business.</p>
                            </div>
                            <div class="col-xs-12 col-sm-4 text-center">
                                <img src="/assets/img/about/eco-friendly.png" alt="eco-friendly services" class="text-center round-img">
                                <h4>Green Services</h4>
                                <p class="text-justify">The Gals Cleaning Service is proud to announce we are a green federally approved janitorial cleaning service. All of our cleaning methods and products are environmentally safe. Our cleaning products have less impact on the environment. Much of our consumables are biodegradable including our vacuum bags. To reduce waste usage we use microfiber clothes and mop heads. They are color coded for specific areas. This avoids any cross contamination. Our microfiber clothes and mop heads are laundered on a regular basis. Please feel free to ask about our green cleaning procedures.</p>
                            </div>
                        </div>
                    </div>
                </section>
                <section>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <h3 class="text-center h1 page-header">Some of Our Clients</h3>
                            </div>
                        </div> 
                    </div> 
                </section>  
                    
                <section class="hero hero-sm">  
                    <div class="gallery">
                        <div class="slide2"><img src="/assets/img/about/clients/brethren.png" alt="Brethren Client"/></div>
                        <div class="slide1"><img src="/assets/img/about/clients/HM.png" alt="HM Client"/></div>
                        <div class="slide3"><img src="/assets/img/about/clients/integrity.png" alt="Integrity Client"/></div>
                        <div class="slide3"><img src="/assets/img/about/clients/MidPennBank.png" alt="MidPennBank Client"/></div>
                    </div>
                </section>
            </main>
        </div>
        
        <?php include('partials/footer.php'); ?>

        <script type="text/javascript" src="/assets/slick/slick.min.js"></script>
        <script type="text/javascript" src="/assets/js/sensei-foot-libs.min.js"></script>
        <script type="text/javascript" src="/assets/js/sensei-bentobox.min.js"></script>

        <script>
            $(document).ready(function(){
                $('.gallery').slick({
                    autoplay: true,
                    autoplaySpeed: 500,
                    speed: 100,
                    slide: true,
                    arrows: true,
                    dots: true,
                });
            });
        </script>      
        
    </body>
	
</html>