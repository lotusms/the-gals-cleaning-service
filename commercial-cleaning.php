<!DOCTYPE html>

<html lang="en-US" class="default-page">
    <head>
		<title>The Gals Cleaning Service | Commercial Cleaning</title>	
        <link rel="canonical" href="https://www.thegalscleaningservice.com/commercial-cleaning/">
        <meta name="description" content="The Gals commercial cleaning services meet a wide range of office cleaning needs for Harrisburg businesses. Whether you work in the medical industry, education field, or industrial environements, our personalized cleaning services can serve your specific cleaning requirements.">

        <?php include('partials/head.php'); ?>

        <meta property="fb:app_id" content=""/>
        <meta property="og:locale" content="en_US">
        <meta property="og:type" content="website">
        <meta property="og:title" content="Commercial Cleaning Services | The Gals Cleaning Services">
        <meta property="og:description" content="The Gals commercial cleaning services meet a wide range of office cleaning needs for Harrisburg businesses. Whether you work in the medical industry, education field, or industrial environements, our personalized cleaning services can serve your specific cleaning requirements.">
        <meta property="og:url" content="https://www.thegalscleaningservice.com/commercial-cleaning/">
        <meta property="og:site_name" content="The Gals Cleaning Services - Commercial Cleaning Services">
        <meta property="og:image" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png" />
        <meta property="og:image:secure_url" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png">

        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="https://www.thegalscleaningservice.com/commercial-cleaning/">
        <meta name="twitter:description" content="The Gals commercial cleaning services meet a wide range of office cleaning needs for Harrisburg businesses. Whether you work in the medical industry, education field, or industrial environements, our personalized cleaning services can serve your specific cleaning requirements.">
        <meta name="twitter:title" content="Commercial Cleaning Services | The Gals Cleaning Services">
        <meta name="twitter:image" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png">

        <script type="application/ld+json">{"@context":"https://schema.org","@type":"WebSite","@id":"https://www.thegalscleaningservice.com/commercial-cleaning/#website","url":"https://www.thegalscleaningservice.com/commercial-cleaning/","name":"The Gals Cleaning Services | Commercial Cleaning Services","potentialAction":{"@type":"SearchAction","target":"https://www.thegalscleaningservice.com/commercial-cleaning/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
    </head>

    <body class="has-hero-intent has-mobi-footer index-template" data-gr-c-s-loaded="true" cz-shortcut-listen="true">        
        <?php include('partials/header.php'); ?>
        
        <div class="site-content">            
            <main>              
                <section class="page-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="page-header">
                                    <h1>Commercial Cleaning</h1>
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <h2>Leading Central Pennsylvania In Commercial Cleaning Services</h2>
                                <p>Staged in the heart of Pennsylvania, The Gals Cleaning Services is your local leader in commercial cleaning and janitorial services. Over the years we have paved a path to deliver unparalleled janitorial services to the people who matter most in your business. We recognize that it is important for your business model to provide a clean and safe work environment for your employees and customers alike. Our friendly staff will ensure that all areas are well kept, sanitized, and cleaned to reduce bacterial growth often found in high-trafficked areas.</p>  
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <p>Many small and large companies throughout Central Pennsylvania trust the Gals Cleaning Services as their commercial cleaning and janitorial services company. They have earned their reputation by being honest, friendly, and engaging; as well as by offering a service that is second to none.</p>    
                            </div>
                        
                            <div class="col-xs-12 col-md-6 text-center">                                
                                <?php include('partials/call-out-box.php'); ?>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="black-bg">
                    <div class="container comm-serv text">
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <h2>Commercial Cleaning Services</h2>
                                <p>Most people think of a hospital when they imagine a place that needs to be immaculately cleaned. That is absolutely true, but that is not all. In reality, all places of business must comply with the health and safety code for that particular establishment. Failure to do so could cause a production halt. We can ensure your workplace is up to code and ready to operate on a daily basis.
                                <br><br>
                                Read more about the code of Health and Safety to familiarize yourself with all that is being governed.
                                </p>
                                <div class="text-center">   
                                    <a href="http://www.statutes.legis.state.tx.us/Docs/HS/htm/HS.341.htm" target="_blank" class="btn btn-primary">See it Here</a>
                                </div>
                            </div>
                        </div>  
                        <div class="serv-block">
                            <div class="row container-row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="row alignstart">
                                        <div class="col-md-3">
                                            <span class="icon-office round-icon"></span>
                                        </div>
                                        <div class="col-md-9">
                                            <h4>Office and Building Cleaning</h4>
                                            <p>Business Cleaning and Office Cleaning increases productivity and customer satisfaction, and it reduces call-offs.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="row alignstart">
                                        <div class="col-md-3">
                                            <span class="icon-location-hotel round-icon"></span>
                                        </div>
                                        <div class="col-md-9">
                                            <h4>Hotel and Resort Cleaning</h4>
                                            <p>Hotel and Resort Cleaning Services enhances repeat guests and minimizes complaints.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row container-row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="row alignstart">
                                        <div class="col-md-3">
                                            <span class="icon-hospital-o round-icon"></span>
                                        </div>
                                        <div class="col-md-9">
                                            <h4>Hospitals and Clinics</h4>
                                            <p>Healthcare surface disinfection is crucial in such environment to help fight the cnstant exposure to germs.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="row alignstart">
                                        <div class="col-md-3">
                                            <span class="icon-cogs round-icon"></span>
                                        </div>
                                        <div class="col-md-9">
                                            <h4>Manufacturing Facility Cleaning</h4>
                                            <p>Industrial cleaning and janitorial services are important to manufaturing plants and construction sites.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row container-row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="row alignstart">
                                        <div class="col-md-3">
                                            <span class="icon-t-shirt round-icon"></span>
                                        </div>
                                        <div class="col-md-9">
                                            <h4>Retail Store Cleaning</h4>
                                            <p>A retail store is considered to generate an immense amount of foot traffic. This is a major flooring stressor.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="row alignstart">
                                        <div class="col-md-3">
                                            <span class="icon-soccer-court round-icon"></span>
                                        </div>
                                        <div class="col-md-9">
                                            <h4>Stadium and Event Cleaning</h4>
                                            <p>Sporting facilities and event hosting facilites are one of the most common places to find spills and overly used restrooms.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row container-row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="row alignstart">
                                        <div class="col-md-3">
                                            <span class="icon-spoon-knife round-icon"></span>
                                        </div>
                                        <div class="col-md-9">
                                            <h4>Restaurant &amp; Bars Cleaning</h4>
                                            <p>Ensuring your restaurant and bar is in pristine conditions will ensure your doors remain open.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="row alignstart">
                                        <div class="col-md-3">
                                            <span class="icon-study round-icon"></span>
                                        </div>
                                        <div class="col-md-9">
                                            <h4>School and University Cleaning</h4>
                                            <p>Keep children in good health and in school is something we can all agree on. Let those little hands touch clean surfaces.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  
                </section>
            </main>
        </div>
        
        <?php include('partials/footer.php'); ?>

        <script type="text/javascript" src="/assets/js/sensei-foot-libs.min.js"></script>
        <script type="text/javascript" src="/assets/js/sensei-bentobox.min.js"></script>
        						    
    </body>
	
</html>