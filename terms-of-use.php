<!DOCTYPE html>

<html lang="en-US" class="default-page">
    <head>
		<title>The Gals Cleaning Service | Terms of Use</title>	
        <link rel="canonical" href="https://www.thegalscleaningservice.com/terms-of-use/">
        <meta name="description" content="The Gals Cleaning Services' website terms of use. We take pride in what we do and ensure nothing short of maximum results. Call today to schedule a free consultation.">

        <?php include('partials/head.php'); ?>


        <meta property="fb:app_id" content=""/>
        <meta property="og:locale" content="en_US">
        <meta property="og:type" content="website">
        <meta property="og:title" content="Terms of Use | The Gals Cleaning Services">
        <meta property="og:description" content="The Gals Cleaning Services' website terms of use. We take pride in what we do and ensure nothing short of maximum results. Call today to schedule a free consultation.">
        <meta property="og:url" content="https://www.thegalscleaningservice.com/terms-of-use/">
        <meta property="og:site_name" content="The Gals Cleaning Services - Terms of Use">
        <meta property="og:image" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png" />
        <meta property="og:image:secure_url" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png">

        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="https://www.thegalscleaningservice.com/terms-of-use/">
        <meta name="twitter:description" content="The Gals Cleaning Services' website terms of use. We take pride in what we do and ensure nothing short of maximum results. Call today to schedule a free consultation.">
        <meta name="twitter:title" content="Terms of Use | The Gals Cleaning Services">
        <meta name="twitter:image" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png">

        <script type="application/ld+json">{"@context":"https://schema.org","@type":"WebSite","@id":"https://www.thegalscleaningservice.com/terms-of-use/#website","url":"https://www.thegalscleaningservice.com/terms-of-use/","name":"The Gals Cleaning Services | Terms of Use","potentialAction":{"@type":"SearchAction","target":"https://www.thegalscleaningservice.com/terms-of-use/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
    </head>

    <body class="has-hero-intent has-mobi-footer index-template" data-gr-c-s-loaded="true" cz-shortcut-listen="true">        
        <?php include('partials/header.php'); ?>
        
        <div class="site-content">            
            <main>              
                <section class="page-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="page-header">
                                    <h1>Terms of Use</h1>
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12" style="padding-bottom: 80px;">
                                <h4>Introduction</h4>

                                <p>By continuing to browse and use this website you are agreeing to comply with and be bound by the terms and conditions described below, which governs The Gals Cleaning Service’s relationship with you in relation to this website. The term “us” or “we” refers to The Gals Cleaning Service whose registered office is www.witmers.com. The term “you” refers to the user or visitor of this website. The use of this website is subject to the following terms of use. </p>

                                <h4>Age</h4>
                                <p>You must be at least 18 years of age to use this website for business operations. Visitors under 18 years of age may use this website for school research material only, if applies. By using this website for business purposes, and by agreeing to these terms and conditions, you warrant and represent that you are at least 18 years of age.</p>

                                <h4>License to Use This Website</h4>
                                <p>
                                    Unless otherwise stated, The Gals Cleaning Service and/or its licensors own the intellectual property rights in the website and material on the website. Subject to the license below, all these intellectual property rights are reserved. You may view, download for caching purposes only, and print pages from the website for your own personal use, subject to the restrictions set out below and elsewhere in these terms and conditions.
                                    <br><br>
                                    You must not:
                                </p>
                                <ul class="list-indent">
                                    <li>republish material from this website (including republication on another website);</li>
                                    <li>sell, rent or sub-license material from the website;</li>
                                    <li>show any material from the website in public;</li>
                                    <li>reproduce, duplicate, copy or otherwise exploit material on this website for a commercial purpose;</li>
                                    <li>edit or otherwise modify any material on the website; or</li>
                                    <li>redistribute material from this website, except for content specifically and expressly made available for redistribution.</li>
                                </ul>

                                <h4>Acceptable Use</h4>
                                <p>You must not use this website in any way that causes, or may cause, damage to the website or impairment of the availability or accessibility of the website; or in any way which is unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or harmful purpose or activity. You must not use this website to copy, store, host, transmit, send, use, publish or distribute any material which consists of (or is linked to any spyware, computer virus, Trojan horse, worm, keystroke logger, rootkit or other malicious computer software. You must not conduct any systematic or automated data collection activities (including without limitation scraping, data mining, data extraction and data harvesting) on or in relation to this website without The Gals Cleaning Service’s express written consent. You must not use this website to transmit or send unsolicited commercial communications. You must not use this website for any purposes related to marketing without The Gals Cleaning Service’s express written consent.</p>

                                <h4>Restricted Access</h4>
                                <p>Access to certain areas of this website is restricted. The Gals Cleaning Service reserves the right to restrict access to the Admin area of this website at The Gals Cleaning Service’s discretion. If The Gals Cleaning Service provides you with a user ID and password to enable you to access restricted areas of this website or other content or services, you must ensure that the user ID and password are kept confidential. The Gals Cleaning Service may disable your user ID and password in The Gals Cleaning Service’s sole discretion without notice or explanation.</p>

                                <h4>User Content</h4>
                                <p>In these terms and conditions, “your user content” means material (including without limitation text, images, audio material, video material and audio-visual material) that you submit to this website, for whatever purpose. You grant to The Gals Cleaning Service a worldwide, irrevocable, non-exclusive, royalty-free license to use, reproduce, adapt, publish, translate and distribute your user content in any existing or future media. You also grant to The Gals Cleaning Service the right to sub-license these rights, and the right to bring an action for infringement of these rights. Your user content must not be illegal or unlawful, must not infringe any third party’s legal rights, and must not be capable of giving rise to legal action whether against you or The Gals Cleaning Service or a third party (in each case under any applicable law). You must not submit any user content to the website that is or has ever been the subject of any threatened or actual legal proceedings or other similar complaint. The Gals Cleaning Service reserves the right to edit or remove any material submitted to this website, or stored on The Gals Cleaning Service’s servers, or hosted or published upon this website. Not withstanding The Gals Cleaning Service′s rights under these terms and conditions in relation to user content, The Gals Cleaning Service does not undertake to monitor the submission of such content to, or the publication of such content on, this website.</p>

                                <h4>No Warranties</h4>
                                <p>This website is provided “as is” without any representations or warranties, express or implied. The Gals Cleaning Service makes no representations or warranties in relation to this website or the information and materials provided on this website. Without prejudice to the generality of the foregoing paragraph, The Gals Cleaning Service does not warrant that: this website will be constantly available, or available at all; or the information on this website is complete, true, accurate or non-misleading. Nothing on this website constitutes, or is meant to constitute, advice of any kind. If you require advice in relation to any legal, financial or medical matter you should consult an appropriate professional.</p>

                                <h4>Limitations Of Liabilities</h4>
                                <p>The Gals Cleaning Service will not be liable to you (whether under the law of contact, the law of torts or otherwise) in relation to the contents of, or use of, or otherwise in connection with, this website:</p>
                                <ul class="list-indent">
                                    <li>to the extent that the website is provided free-of-charge, for any direct loss;</li>
                                    <li>for any indirect, special or consequential loss; or</li>
                                    <li>for any business losses, loss of revenue, income, profits or anticipated savings, loss of contracts or business relationships, loss of reputation or goodwill, or loss or corruption of information or data.</li>
                                </ul>
                                <p>These limitations of liability apply even if The Gals Cleaning Service has been expressly advised of the potential loss.</p>

                                <h4>Exceptions</h4>
                                <p>Nothing in this website disclaimer will exclude or limit any warranty implied by law that it would be unlawful to exclude or limit; and nothing in this website disclaimer will exclude or limit The Gals Cleaning Service’s liability in respect of any:</p>
                                <ul class="list-indent">
                                    <li>death or personal injury caused by The Gals Cleaning Service’s negligence;</li>
                                    <li>fraud or fraudulent misrepresentation on the part of The Gals Cleaning Service; or</li>
                                    <li>matter which it would be illegal or unlawful for The Gals Cleaning Service to exclude or limit, or to attempt or purport to exclude or limit, its liability.</li>
                                </ul>

                                <h4>Reasonableness</h4>
                                <p>By using this website, you agree that the exclusions and limitations of liability set out in this website disclaimer are reasonable. If you do not think they are reasonable, you must not use this website.</p>

                                <h4>Other Parties</h4>
                                <p>
                                    You accept that, as a limited liability entity, The Gals Cleaning Service has an interest in limiting the personal liability of its officers and employees. You agree that you will not bring any claim personally against The Gals Cleaning Service’s officers or employees in respect of any losses you suffer in connection with the website.
                                    <br><br>
                                    Without prejudice to the foregoing paragraph, you agree that the limitations of warranties and liability set out in this website disclaimer will protect The Gals Cleaning Service’s officers, employees, agents, subsidiaries, successors, assigns and sub-contractors as well as The Gals Cleaning Service.
                                </p>

                                <h4>Unenforceable Provisions</h4>
                                <p>If any provision of this website disclaimer is, or is found to be, unenforceable under applicable law, that will not affect the enforcement of the other provisions of this website disclaimer.</p>

                                <h4>Indemnity</h4>
                                <p>You hereby indemnify The Gals Cleaning Service and undertake to keep The Gals Cleaning Service indemnified against any losses, damages, costs, liabilities and expenses (including without limitation legal expenses and any amounts paid by The Gals Cleaning Service to a third party in settlement of a claim or dispute on the advice of The Gals Cleaning Service’s legal advisers) incurred or suffered by The Gals Cleaning Service arising out of any breach by you of any provision of these terms and conditions, or arising out of any claim that you have breached any provision of these terms and conditions.</p>

                                <h4>Breaches Of These Terms And Conditions</h4>
                                <p>Without prejudice to The Gals Cleaning Service’s other rights under these terms and conditions, if you breach these terms and conditions in any way, The Gals Cleaning Service may take such action as The Gals Cleaning Service deems appropriate to deal with the breach, including suspending your access to the website, prohibiting you from accessing the website, blocking computers using your IP address from accessing the website, contacting your Internet service provider to request that they block your access to the website and/or bringing court proceedings against you.</p>

                                <h4>Variation</h4>
                                <p>The Gals Cleaning Service may revise these terms and conditions from time-to-time. Revised terms and conditions will apply to the use of this website from the date of the publication of the revised terms and conditions on this website. Please check this page regularly to ensure you are familiar with the current version.</p>

                                <h4>Assignment</h4>
                                <p>The Gals Cleaning Service may transfer, sub-contract or otherwise deal with The Gals Cleaning Service’s rights and/or obligations under these terms and conditions without notifying you or obtaining your consent. You may not transfer, sub-contract or otherwise deal with your rights and/or obligations under these terms and conditions.</p>

                                <h4>Severability</h4>
                                <p>If a provision of these terms and conditions is determined by any court or other competent authority to be unlawful and/or unenforceable, the other provisions will continue in effect. If any unlawful and/or unenforceable provision would be lawful or enforceable if part of it were deleted, that part will be deemed to be deleted, and the rest of the provision will continue in effect.</p>

                                <h4>Entire Agreement</h4>
                                <p>These terms and conditions constitute the entire agreement between you and The Gals Cleaning Service in relation to your use of this website, and supersede all previous agreements in respect of your use of this website.</p>

                                <h4>Law &amp; Jurisdiction</h4>
                                <p>These terms and conditions will be governed by and construed in accordance with your local state’s law and federal laws whenever they may override, and any disputes relating to these terms and conditions will be subject to the non-exclusive jurisdiction of the courts.</p>

                                <h4>Your Consent</h4>
                                <p>By using our site, you consent to our web site privacy policy.</p>

                                <h4>Changes to our Terms of Use</h4>
                                <p>
                                    If we decide to change our terms of use policy, we will post those changes on this page.
                                    This policy was last modified on 01/10/2019
                                </p>

                                <h4>Contacting Us</h4>
                                <p>If there are any questions regarding this terms of use you may contact us using the information below.</p>


                                <span><a href="http://www.thegalscleaningservice.com/">www.thegalscleaningservice.com</a></span><br>
                                <span>112 E. Columbia Rd. Enola, PA 17025</span><br>
                                <span><a href="mailto:thegalscleaningservice@yahoo.com">thegalscleaningservice@yahoo.com</a></span><br>
                                <span>Phone: (717) 645-9633</span><br>
                            </div>
                        </div>
                    </div>
                </section>    
            </main>
        </div>
        
        <?php include('partials/footer.php'); ?>

        <script type="text/javascript" src="/assets/js/sensei-foot-libs.min.js"></script>
        <script type="text/javascript" src="/assets/js/sensei-bentobox.min.js"></script>
        
    </body>
	
</html>