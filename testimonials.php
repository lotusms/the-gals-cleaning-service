<!DOCTYPE html>

<html lang="en-US" class="default-page">
    <head>
		<title>The Gals Cleaning Service | Testimonials</title>	
        <link rel="canonical" href="https://www.thegalscleaningservice.com/testimonials/">
        <meta name="description" content="At The Gals Cleaning Services, we take pride in our professional and personalized cleaning services and ensure nothing short of maximum results. See what some of our customers have to say.">

        <?php include('partials/head.php'); ?>


        <meta property="fb:app_id" content=""/>
        <meta property="og:locale" content="en_US">
        <meta property="og:type" content="website">
        <meta property="og:title" content="Testimonials | The Gals Cleaning Services">
        <meta property="og:description" content="At The Gals Cleaning Services, we take pride in our professional and personalized cleaning services and ensure nothing short of maximum results. See what some of our customers have to say.">
        <meta property="og:url" content="https://www.thegalscleaningservice.com/testimonials/">
        <meta property="og:site_name" content="The Gals Cleaning Services - Testimonials">
        <meta property="og:image" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png" />
        <meta property="og:image:secure_url" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png">

        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="https://www.thegalscleaningservice.com/testimonials/">
        <meta name="twitter:description" content="At The Gals Cleaning Services, we take pride in our professional and personalized cleaning services and ensure nothing short of maximum results. See what some of our customers have to say.">
        <meta name="twitter:title" content="Testimonials | The Gals Cleaning Services">
        <meta name="twitter:image" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png">

        <script type="application/ld+json">{"@context":"https://schema.org","@type":"WebSite","@id":"https://www.thegalscleaningservice.com/testimonials/#website","url":"https://www.thegalscleaningservice.com/testimonials/","name":"The Gals Cleaning Services | Testimonials","potentialAction":{"@type":"SearchAction","target":"https://www.thegalscleaningservice.com/testimonials/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick-theme.css"/>

    </head>

    <body class="has-hero-intent has-mobi-footer index-template" data-gr-c-s-loaded="true" cz-shortcut-listen="true">        
        <?php include('partials/header.php'); ?>
        
        <div class="site-content">            
            <main>              
                <section class="page-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="page-header">
                                    <h1>Testimonials</h1>
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p>Surely anyone can speak highly of themselves. It’s part of being proud of what you do. But for a change, let’s just give the floor to our clients and see what they have to say about us!</p>  
                            </div>
                        </div>
                    </div>
                </section>    
                <section class="black-bg" style="max-height: 300px;"> 
                    <div class=" hero hero-med">
                        <div class="gallery">
                            <div class="slide2">
                                <p>Having been a branch manager for many years at several different banks, I can honestly say that The Gals Cleaning Service is a truly unique cleaning service. Rarely any issues arise. When I would call due to a holiday schedule change, Angela calls back within minutes. Our branches are very clean. We know we can count on The Gals Cleaning Service if we have food drives or are donating to the United Way. The same team comes into clean. This really means a lot to us, being a bank. Angela I know you will continue to grow. Thank you for everything.</p>
                                <h5>MidPenn Bank</h5>
                            </div>
                            <div class="slide1">
                                <p>Having been in the building business for over 20 years, I can honestly say that hiring The Gals Cleaning Service 5 years ago has been a positive decision and has contributed to our success in building quality homes. They have been able to maintain and deliver quality work to all of our new homes. Their attention to detail has helped our superintendents to identify problems with last minute details.</p>
                                <h5>Richmar Builders</h5>
                            </div>
                        </div>
                    </div>
                </section>
            </main>
        </div>
        
        <?php include('partials/footer.php'); ?>

        <script type="text/javascript" src="/assets/slick/slick.min.js"></script>
        <script type="text/javascript" src="/assets/js/sensei-foot-libs.min.js"></script>
        <script type="text/javascript" src="/assets/js/sensei-bentobox.min.js"></script>

        <script>
            $(document).ready(function(){
                $('.gallery').slick({
                    autoplay: true,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    autoplaySpeed: 500,
                    speed: 100,
                    slide: true,
                    arrows: true,
                    dots: true,
                });
            });
        </script>      
        
    </body>
	
</html>