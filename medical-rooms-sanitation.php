<!DOCTYPE html>

<html lang="en-US" class="default-page">
    <head>
		<title>The Gals Cleaning Service | Medical Rooms Sanitation</title>	
        <link rel="canonical" href="https://www.thegalscleaningservice.com/medical-rooms-sanitation/">
        <meta name="description" content="">

        <?php include('partials/head.php'); ?>


        <meta property="fb:app_id" content=""/>
        <meta property="og:locale" content="en_US">
        <meta property="og:type" content="website">
        <meta property="og:title" content="Medical Rooms Sanitation | The Gals Cleaning Services">
        <meta property="og:description" content="At The Gals Cleaning Services, we deliver personalized commercial cleaning services to office buildings, medical and industrial facilties. We take pride in what we do and ensure nothing short of maximum results. Call today to schedule a free consultation.">
        <meta property="og:url" content="https://www.thegalscleaningservice.com/medical-rooms-sanitation/">
        <meta property="og:site_name" content="The Gals Cleaning Services - Medical Rooms Sanitation">
        <meta property="og:image" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png" />
        <meta property="og:image:secure_url" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png">

        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="https://www.thegalscleaningservice.com/medical-rooms-sanitation/">
        <meta name="twitter:description" content="At The Gals Cleaning Services, we deliver personalized commercial cleaning services to office buildings, medical and industrial facilties. We take pride in what we do and ensure nothing short of maximum results. Call today to schedule a free consultation.">
        <meta name="twitter:title" content="Medical Rooms Sanitation | The Gals Cleaning Services">
        <meta name="twitter:image" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png">

        <script type="application/ld+json">{"@context":"https://schema.org","@type":"WebSite","@id":"https://www.thegalscleaningservice.com/medical-rooms-sanitation/#website","url":"https://www.thegalscleaningservice.com/medical-rooms-sanitation/","name":"The Gals Cleaning Services | Medical Rooms Sanitation","potentialAction":{"@type":"SearchAction","target":"https://www.thegalscleaningservice.com/medical-rooms-sanitation/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
    </head>

    <body class="has-hero-intent has-mobi-footer index-template" data-gr-c-s-loaded="true" cz-shortcut-listen="true">        
        <?php include('partials/header.php'); ?>
        
        <div class="site-content">            
            <main>              
                <section class="page-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="page-header">
                                    <h1>Medical Rooms Sanitation</h1>
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <h2>Disease Control Support In All Of Our Efforts</h2>
                                <p>Nationwide, government agencies increase the fight against communicable disease. Laws are in place, not only in the healthcare industry, but in others as well that require all establishments to follow certain sanitation guidelines to reduce outbreaks. Hospitals, outpatient clinics, and dental offices are subjected to a more scrutinized checklist considering that they are more exposed to a communicable disease.</p>  
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <p>We understand the perilous nature of healthcare surfaces and we ensure a more strict cleaning procedure is in place. The Gals Cleaning Services Medical Rooms Cleaning Services pledges to perform stringent cleaning methods and disinfecting practices to ensure the facility is within the Center of Disease Control’s Laws and Regulations Governing the Control of Communicable Diseases. 
                                <br/> <br/>
                                Read more about the Laws and Regulations Governing the Control of Communicable Diseases to familiarize yourself with all that is being governed.</p>   
                                <div class="text-center">   
                                    <a href="http://www.statutes.legis.state.tx.us/Docs/HS/htm/HS.341.htm" target="_blank" class="btn btn-primary">See it Here</a>
                                </div> 
                            </div>
                        
                            <div class="col-xs-12 col-md-6 text-center">
                                <?php include('partials/call-out-box.php'); ?>
                            </div>
                        </div>
                    </div>
                </section>
            </main>
        </div>
        
        <?php include('partials/footer.php'); ?>

        <script type="text/javascript" src="/assets/js/sensei-foot-libs.min.js"></script>
        <script type="text/javascript" src="/assets/js/sensei-bentobox.min.js"></script>
        
    </body>
	
</html>