<!DOCTYPE html>

<html lang="en-US" class="">
    <head>
		<title>The Gals Cleaning Service</title>	
        <link rel="canonical" href="https://www.thegalscleaningservice.com/">
        <meta name="description" content="The Gals Cleaning Services provides 24/7 personalized commercial cleaning services for offices, commercial, and industrial facilities in the Harrisburg area.">

        <?php include('partials/head.php'); ?>

        <link rel="stylesheet" type="text/css" href="/assets/slick/slick.css"/>

		<meta property="fb:app_id" content=""/>
        <meta property="og:locale" content="en_US">
        <meta property="og:type" content="website">
        <meta property="og:title" content="Commercial Cleaning Services &amp; Office Cleaning | The Gals Cleaning Services">
        <meta property="og:description" content="The Gals Cleaning Services provides 24/7 personalized commercial cleaning services for offices, commercial, and industrial facilities in the Harrisburg area.">
        <meta property="og:url" content="https://www.thegalscleaningservice.com/">
        <meta property="og:site_name" content="The Gals Cleaning Services">
		<meta property="og:image" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png" />
        <meta property="og:image:secure_url" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png">

        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="https://www.thegalscleaningservice.com/">
        <meta name="twitter:description" content="The Gals Cleaning Services provides 24/7 personalized commercial cleaning services for offices, commercial, and industrial facilities in the Harrisburg area.">
        <meta name="twitter:title" content="Commercial Cleaning Services &amp; Office Cleaning | The Gals Cleaning Services">
        <meta name="twitter:image" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png">

        <script type="application/ld+json">{"@context":"https://schema.org","@type":"WebSite","@id":"https://www.thegalscleaningservice.com/#website","url":"https://www.thegalscleaningservice.com","name":"The Gals Cleaning Services","potentialAction":{"@type":"SearchAction","target":"https://www.thegalscleaningservice.com/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>

        <script>
            $(document).ready(function() {

				var request;

              	$("#quoteForm").submit(function(event){
	                event.preventDefault();

					// Abort any pending request
				    if (request) {
				        request.abort();
				    }

					// setup some local variables
    				var $form = $(this);

					// Let's select and cache all the fields
    				var $inputs = $form.find("input, select, button, textarea");

					// Serialize the data in the form
					var serializedData = $form.serialize();

					// Let's disable the inputs for the duration of the Ajax request.
					// Note: we disable elements AFTER the form data has been serialized.
					// Disabled form elements will not be serialized.
					// $inputs.prop("disabled", true);

	                request = $.ajax({
        				url: "homeMail",
	                  	type: "post",
	                  	data: serializedData
	                });

					// Callback handler that will be called on success
				    request.done(function (response, textStatus, jqXHR){

					    if (response.status == 0) {
					        // Success! Do something successful, like show success modal
                            jQuery.noConflict();
					        $('#successEmail').modal('show');
					    } else {
					        // Oh no, failure - notify the user
                            jQuery.noConflict();
                            $('#failEmail').modal('show');
					    }

                        $( "#failBtn" ).click(function() {
                            jQuery.noConflict();
                            $('#failEmail').modal('hide');
                        });

                        $( "#passBtn" ).click(function() {
                            window.location.reload();
                        });

				    });

				    // Callback handler that will be called on failure
				    request.fail(function (jqXHR, textStatus, errorThrown){
						// Say you have a <div class="message"></div> somewhere, maybe under your form
    					//$('.message').html('Error: ' + textStatus + ', ' + errorThrown)
				    });

					// Callback handler that will be called regardless
				    // if the request failed or succeeded
				    request.always(function () {
				        // Reenable the inputs
				        $inputs.prop("disabled", false);
				    });
              	});
        	});
      	</script>
    </head>

    <body class="has-hero-intent has-mobi-footer index-template" data-gr-c-s-loaded="true" cz-shortcut-listen="true">       
        <?php include('partials/header.php'); ?>
        
        <div class="site-content">            
            <main>              
                <div class="hero__form container-fluid">
                    <div class="row">
                        <div class="col-md-8 form-heading">
                            <h1>Office Cleaning Services in Harrisburg, PA</h1>
                            <ul>
                                <li><span><i class="fas fa-check"></i></span> Personalized cleaning services for your office</li>
                                <li><span><i class="fas fa-check"></i></span> Family Owned &amp; Operated</li>
                                <li><span><i class="fas fa-check"></i></span> Safe &amp; eco-friendly products</li>
                                <li><span><i class="fas fa-check"></i></span> Guaranteed Quality &amp; quick response</li>
                            </ul>

                            <p>Our office cleaning services are second to none. We take every project seriously and personal. Our clients can always trust us that we will provide the absolute best results and go gret lengths to meet their demands. </p>
                        </div>
                        <div class="col-md-4 form-container">
                            <form id="quoteForm" method="POST" autocomplete="off">
                                <div class="form-group">
                                    <label for="zip">Zip Code</label>
                                    <input type="text" class="form-control" id="zip" name="zip" />
                                </div>
                                <div class="form-group">
                                    <label for="facility">Facility Name</label>
                                    <input type="text" class="form-control" id="facility" name="facility" />
                                </div>
                                <div class="form-group">
                                    <label for="interested">Interested In</label>
                                    <select class="form-control" id="interested" name="interested">
                                        <option value=""></option>
                                        <option value="Commercial Cleaning">Commercial Cleaning</option>
                                        <option value="Medical Room Sanitation">Medical Room Sanitation</option>
                                        <option value="Industrial Cleaning">Industrial Cleaning</option>
                                        <option value="Floor &amp; Carpeting Cleaning">Floor &amp; Carpeting Cleaning</option>
                                        <option value="Window Cleaning Services">Window Cleaning Services</option>
                                        <option value="School &amp; Child Care Cleaning">School &amp; Child Care Cleaning</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="frequency">Frequency of Service</label>
                                    <select class="form-control" id="frequency" name="frequency">
                                            <option value=""></option>
                                            <option value="Daily">Daily</option>
                                            <option value="6X Per Week">6X Per Week</option>
                                            <option value="5X Per Week">5X Per Week</option>
                                            <option value="4X Per Week">4X Per Week</option>
                                            <option value="3X Per Week">3X Per Week</option>
                                            <option value="2X Per Week">2X Per Week</option>
                                            <option value="Once a Week">Once a Week</option>
                                            <option value="One-Time Service">One-Time Service</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="name">Your Name</label>
                                    <input type="text" class="form-control" id="name" name="name"/>
                                </div>
                                <div class="form-group">
                                    <label for="name">Your Email</label>
                                    <input type="email" class="form-control" id="email" name="email" />
                                </div>
                                <div class="form-group">
                                    <label for="phone">Phone Number</label>
                                    <input type="text" class="form-control" id="phone" name="phone" />
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-lg-8">
                                        <img id="captcha" src="/securimage/securimage_show.php" alt="CAPTCHA Image" />
                                        <br/>
                                        <input type="text" name="captcha_code" size="10" maxlength="6" autocomplete="off" />
                                        <br/>
                                        <a href="#" id="secureText" onclick="document.getElementById('captcha').src = '/securimage/securimage_show.php?' + Math.random(); return false">
                                            <small class="refresh-captcha">Refresh Image</small>
                                        </a>
                                    </div>
                                    <div class="col-md-12 col-lg-4">
                                        <button type="submit" class="btn btn-brand-alt" id="btnSubmit">Submit</button>  
                                    </div>
                                </div>     
                            </form>
                        </div>
                    </div>
                </div>  
                <section id="hero" class="hero hero--no-content hero--gallery hero--fullheight revealable revealed" style="height: 994px;">   
                    <div class="gallery gallery--fit">
                        <div class="gallery__item slide2" style="background-image: url('/assets/img/banners/banner2.png');"></div>
                    </div>
                </section>
                <section class="features clearfix">
                    <div class="col-lg-12">
                        <div class="container">
                            <div class="col-lg-12 text-center">
                                <h2 class="h1">Harrisburg’s Experts in Janitorial Services</h2>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="feature feature1">
                                        <h3>Deep Cleaning</h3>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="feature feature2">
                                        <h3>Eco Friendly</h3>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="feature feature3">
                                        <h3>Medical Cleaning</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="parallax-box image-parallax-box white-bg">
                    <div class="row custom_block">
                        <div class="col-xs-12 col-md-4">
                            <h4>Relax</h4>
                            <p>You’re in great hands!</p>
                            <h5>Our staff takes pride in what they do.</h5>
                            <em>Having been a branch manager for many years at several different banks, I can honestly say that The Gals Cleaning Service is a truly unique cleaning service. Rarely any issues arise. When I would call due to a holiday schedule change, Angela calls back within minutes. Our branches are very clean. We know we can count on The Gals Cleaning Service if we have food drives or are donating to the United Way. The same team comes into clean. This really means a lot to us, being a bank. Angela I know you will continue to grow. Thank you for everything. – <span class="author">K. Mid Penn Bank.</span></em>
                        </div>
                    
                        <div class="col-xs-12 col-md-8">
                            <div class="biglogo"></div>
                        </div>
                    </div> 
                </section>

                <section class="parallax-box cleaning-parallax-box white-bg services">
                    <div class="container">
                        <div class="row flex-center">
                            <div class="col-lg-12">
                                <h2>Some Of Our Services</h2>
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <div class="service-box">
                                    <figure class="icon">
                                        <img src="/assets/img/services/commercial-cleaning.png" alt="Commercial Cleaning" class="round-img">
                                    </figure>
                                    <div class="service-box_body">
                                        <h4 class="title">Commercial Cleaning</h4>
                                        <div class="service-box_txt">Our janitorial services can leave your workplace clean, healthy, and ready for operations. Let us handle the dirty work.</div>
                                        <div class="btn-align">
                                            <a href="/commercial-cleaning.php" class="btn btn-service" target="_self">read more</a>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <div class="service-box">
                                    <figure class="icon">
                                        <img src="/assets/img/services/post-construction.png" alt="Post-Construction Cleanup" class="round-img">
                                    </figure>
                                    <div class="service-box_body">
                                        <h4 class="title">Industrial Cleaning</h4>
                                        <div class="service-box_txt">Construction sites generate more dust and debri than anything else. We can ensure the site is cleaned for the next day.
                                        </div>
                                        <div class="btn-align">
                                            <a href="/industrial-cleaning.php" class="btn btn-service" target="_self">read more</a>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <div class="service-box">
                                    <figure class="icon">
                                        <img src="/assets/img/services/floor-care.jpg" alt="Floor Care" class="round-img">
                                    </figure>
                                    <div class="service-box_body">
                                        <h4 class="title">Floor Care</h4>
                                        <div class="service-box_txt">Whether it is carpet, tile, or wood flooring, foot traffic can quickly deteriorate office floors. We can reduce that damage.
                                        </div>
                                        <div class="btn-align">
                                        <a href="/floor-care-services.php" class="btn btn-service" target="_self">read more</a>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <div class="service-box">
                                    <figure class="icon">
                                        <img src="/assets/img/services/window-cleaning.jpg" alt="Window Cleaning" class="round-img">
                                    </figure>
                                    <div class="service-box_body">
                                        <h4 class="title">Window Cleaning</h4>
                                        <div class="service-box_txt">Nothing can degrade the clean look of a space like a dirty window. Our job is to make your storefront inviting and streak-free.
                                        </div>
                                        <div class="btn-align">
                                        <a href="/window-cleaning-services.php" class="btn btn-service" target="_self">read more</a>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </div> 
                    </div>
                </section>
            </main>
        </div>


        <div id="successEmail" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body text-center">
                        <i class="fas fa-thumbs-up" style="color: #4caf50; border-radius: 100%; border: 5px solid #4caf50; padding: 20px; margin-bottom: 25px; font-size: 50px;">
                        </i>
                        <p style="color: #4caf50;font-weight: 500;"> Thank you for your message! Someone will contact you very soon.</p>
                    </div>
                    <div class="modal-footer text-center">
                        <button id="passBtn" type="button" class="btn btn-success btn-block" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="failEmail" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body text-center">
                        <i class="fas fa-exclamation-triangle" style="color: #dc3545; border-radius: 100%; border: 5px solid #dc3545;  padding: 20px; margin-bottom: 25px; font-size: 50px;"></i>
                        <p style="color: #dc3545;font-weight: 500;">Something went wrong. Review your form and make sure nothing was missed.</p>
                    </div>
                    <div class="modal-footer text-center">
                        <button id="failBtn" type="button" class="btn btn-danger btn-block" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        
        <?php include('partials/footer.php'); ?>

        <script type="text/javascript" src="/assets/slick/slick.min.js"></script>
        <script type="text/javascript" src="/assets/js/sensei-foot-libs.min.js"></script>
        <script type="text/javascript" src="/assets/js/sensei-bentobox.min.js"></script>
        
        <script>
            $(document).ready(function(){
                $('.gallery').slick({
                    autoplay: true,
                    autoplaySpeed: 5000,
                    speed: 3000,
                    adaptiveHeight: true,
                    arrows: true,
                    dots: true,
                    fade: true
                });
            });
        </script>      
        						    
        <script type="text/javascript" id="">window.dataLayer=window.dataLayer||[];function gtag(){dataLayer.push(arguments)}gtag("js",new Date);gtag("config","AW-981673407");</script>

        <script type="text/javascript" id="">gtag("config","AW-981673407/soynCJefkIYBEL_LjNQD",{phone_conversion_number:"(717) 645-9633"});</script>
    </body>
	
</html>