<!DOCTYPE html>

<html lang="en-US" class="default-page">
    <head>
		<title>The Gals Cleaning Service | Window Cleaning Services</title>	
        <link rel="canonical" href="https://www.thegalscleaningservice.com/window-cleaning-services/">
        <meta name="description" content="At The Gals Cleaning Services, we deliver personalized window cleaning services to office buildings, medical and industrial facilties. We take pride in what we do and ensure nothing short of maximum results. Call today to schedule a free consultation.">

        <?php include('partials/head.php'); ?>


        <meta property="fb:app_id" content=""/>
        <meta property="og:locale" content="en_US">
        <meta property="og:type" content="website">
        <meta property="og:title" content="Window Cleaning Services | The Gals Cleaning Services">
        <meta property="og:description" content="At The Gals Cleaning Services, we deliver personalized window cleaning services to office buildings, medical and industrial facilties. We take pride in what we do and ensure nothing short of maximum results. Call today to schedule a free consultation.">
        <meta property="og:url" content="https://www.thegalscleaningservice.com/window-cleaning-services/">
        <meta property="og:site_name" content="The Gals Cleaning Services - Window Cleaning Services">
        <meta property="og:image" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png" />
        <meta property="og:image:secure_url" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png">

        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="https://www.thegalscleaningservice.com/window-cleaning-services/">
        <meta name="twitter:description" content="At The Gals Cleaning Services, we deliver personalized window cleaning services to office buildings, medical and industrial facilties. We take pride in what we do and ensure nothing short of maximum results. Call today to schedule a free consultation.">
        <meta name="twitter:title" content="Window Cleaning Services | The Gals Cleaning Services">
        <meta name="twitter:image" content="https://www.thegalscleaningservice.com/assets/img/Facebook-Share-Card.png">

        <script type="application/ld+json">{"@context":"https://schema.org","@type":"WebSite","@id":"https://www.thegalscleaningservice.com/window-cleaning-services/#website","url":"https://www.thegalscleaningservice.com/window-cleaning-services/","name":"The Gals Cleaning Services | Window Cleaning Services","potentialAction":{"@type":"SearchAction","target":"https://www.thegalscleaningservice.com/window-cleaning-services/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
    </head>

    <body class="has-hero-intent has-mobi-footer index-template" data-gr-c-s-loaded="true" cz-shortcut-listen="true">        
        <?php include('partials/header.php'); ?>
        
        <div class="site-content">            
            <main>              
                <section class="page-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="page-header">
                                    <h1>Window Cleaning Services</h1>
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <h2>Your Storefront Matters</h2>
                                <p>The conditions in which a your business’ windows are can make a difference in whether your office is percieved as a dull and lifeless office or a vibrant and lively office. This is a great deal. Windows collect dust, dirt, and spider webs over time and neglecting them can only lead to a negative impression on your customers. Similarly, your staff’s mood can be affected as well and productivity can suffer.
                                <br/><br/>
                                Although cleaning your windows may seem an easy task to take on, it is not always the case. In many occasions, having an employee clean a high window can put your employee and your business at risk. Should the employee suffer a fall, it can lead to serious complications and a long term of workers compensation and retribution. </p>  
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <p>Our trained professionals are experts in window cleaning and have the most effective technology and equipment to ensure your windows are cleaned safely. In addition, we don’t use a simple solution of water and soap or household cleaning products. The Gals use Unger technology to clean windows up to three stories high without the use of ladders. Unger also removes water impurities that cause streaks and water residues.  
                                <br/> <br/>
                                Read more about the Unger technology.</p>   
                                <div class="text-center">   
                                    <a href="https://www.ungerglobal.com/en/" target="_blank" class="btn btn-primary">See it Here</a>
                                </div> 
                            </div>
                        
                            <div class="col-xs-12 col-md-6 text-center">
                                <?php include('partials/call-out-box.php'); ?>
                            </div>
                        </div>
                    </div>
                </section>
            </main>
        </div>
        
        <?php include('partials/footer.php'); ?>

        <script type="text/javascript" src="/assets/js/sensei-foot-libs.min.js"></script>
        <script type="text/javascript" src="/assets/js/sensei-bentobox.min.js"></script>
        
    </body>
	
</html>