<?php
session_start();

$msg = '';


use PHPMailer\PHPMailer\PHPMailer;

require './mail/PHPMailer.php';
require './mail/Exception.php';
require './mail/SMTP.php';
require './mail/PHPMailerAutoload.php';

include_once $_SERVER['DOCUMENT_ROOT'] . '/securimage/securimage.php';

$securimage = new Securimage();

//Don't run this unless we're handling a form submission
if (array_key_exists('email', $_POST)) {

    date_default_timezone_set('Etc/UTC');

    //Create a new PHPMailer instance
    $mail = new PHPMailer;

    $mail->SMTPDebug = 0;                                       // Enable verbose debug output
    $mail->isSMTP();                                            // Set mailer to use SMTP
    $mail->Host = 'smtp.mail.yahoo.com';                        // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                                     // Enable SMTP authentication
    $mail->Username = 'thegalscleaningservice@yahoo.com';       // SMTP username
    $mail->Password = 'Percey112';                              // SMTP password
    $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                          // TCP port to connect to
    // $mail->Debugoutput = 'text';

    $mail->setFrom('lotusms@outlook.com', 'Mailer');
    $mail->addAddress('lotusms@outlook.com', 'Angela Martin');

	$email = isset($_POST['email']) ? $_POST['email'] : null;
	$name = isset($_POST['name']) ? $_POST['name'] : null;
	$phone = isset($_POST['phone']) ? $_POST['phone'] : null;
	$comments = isset($_POST['comments']) ? $_POST['comments'] : null;


    if ($mail->addReplyTo($_POST['email'], $_POST['name'])) {
        $mail->Subject = 'Contact From The Gals Online';
        $mail->isHTML(true);
        $mail->Body = <<<EOT
<div style="width:100%">
<div><label style="color: #044F69; font-weight:bold">Email:</label> <span>{$_POST['email']}</span></div>
<div><label style="color: #044F69; font-weight:bold">Name:</label> <span>{$_POST['name']}</span></div>
<div><label style="color: #044F69; font-weight:bold">Phone:</label> <span>{$_POST['phone']}</span></div>
<div><label style="color: #044F69; font-weight:bold">Message:</label> <span>{$_POST['comments']}</span></div>
</div>
EOT;

        if ($securimage->check($_POST['captcha_code']) == false) {
            $response = [
                'status'=> 1,
                'msg'   => 'CAPTCHA test failed!'
            ];
        } else {
            //Send the message, check for errors
            if (!$mail->send()) {
                // Generate a response in this failure case, including a message and a status flag
                $response = [
                    'status'=> 1,
                    'msg'   => 'Sorry, something went wrong. Please try again later.'
                ];
            } else {
                // Generate a response in the success case, including a message and a status flag
                $response = [
                    'status'=> 0,
                    'msg'   => 'Success!'
                ];
            }
        }
    } else {
        // Generate a response in this failure case, including a message and a status flag
        $response = [
            'status'=> 1,
            'msg'   => 'Invalid email address, message ignored.'
        ];
    }
}
// Add the response to the session, so that it will be available after reload
$_SESSION['response'] = $response;

// Finally display the response as JSON so calling JS can see what happened
header('Content-Type: application/json');
echo json_encode($response);

?>
