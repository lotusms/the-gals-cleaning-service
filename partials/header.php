
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KNXLH6M" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '908078312672006',
            xfbml      : true,
            version    : 'v2.6'
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<header class="site-header">
    <a href="#main-content" class="skip">Skip to main content</a>
    <div class="site-header-desktop">
        <div class="site-header-desktop-primary site-header-desktop-primary--floatable" data-header-sticky="" style="">
            <div class="container">                      
                <div class="site-logo">
                    <a class="site-logo__btn" href="/">
                        The Gals Cleaning Service
                    </a>
                </div>         
                <nav class="site-nav">            
                    <ul class="site-nav-menu"> 
                        <li><a href="/">HOME</a></li>
                        <li class="site-nav-submenu" tabindex="0">
                            <span>Janitorial Services<span class="sr-only">, Sub menu expanded</span></span>
                            <div>
                                <ul>
                                    <li><a href="/commercial-cleaning.php">Commercial Cleaning</a></li>
                                    <li><a href="/medical-rooms-sanitation.php">Medical Rooms Sanitation</a></li>
                                    <li><a href="/industrial-cleaning.php">Industrial Cleaning</a></li>
                                    <li><a href="/floor-care-services.php">Floor Care Services</a></li>
                                    <li><a href="/window-cleaning-services.php">Window Cleaning Services</a></li>
                                    <li><a href="/school-child-care-cleaning.php">School &amp; Child Care Cleaning</a></li>
                                </ul>
                            </div>
                        </li>
                        <!-- <li><a href="/outreach.php">Outreach</a></li> -->
                        <li class="site-nav-submenu" tabindex="0">
                            <span>About<span class="sr-only">, Sub menu expanded</span></span>
                            <div>
                                <ul>
                                    <li><a href="/about.php">About</a></li>
                                    <li><a href="/testimonials.php">Testimonials</a></li>
                                </ul>
                            </div>
                        </li>
                        <li><a href="/contact.php">Contact</a></li>
                        <li><a href="/quote-request.php">Quote Request</a></li>
                    </ul>
                </nav>  
            </div>
        </div>
    </div>
    <div class="site-header-mobi">
        <button type="button" class="nav-toggle-btn">
            <span class="sr-only">Toggle Navigation</span>
            <span class="nav-toggle-btn__line"></span>
            <span class="nav-toggle-btn__line"></span>
            <span class="nav-toggle-btn__line"></span>
        </button>
        
        <div class="site-logo">
            <a class="site-logo__btn" href="/">
                The Gals Cleaning Service
            </a>
        </div>      

        <div class="site-header-mobi-panel">
            <div class="site-header-mobi-panel__inner">                      
                <nav class="site-nav">
                    <ul class="site-nav-menu"> 
                        <li><a href="/">HOME</a></li>
                        <li class="site-nav-submenu" tabindex="0">
                            <span>Janitorial Services<span class="sr-only">, Sub menu expanded</span></span>
                            <div>
                                <ul>
                                    <li><a href="/commercial-cleaning.php">Commercial Cleaning</a></li>
                                    <li><a href="/medical-rooms-sanitation.php">Medical Rooms Sanitation</a></li>
                                    <li><a href="/industrial-cleaning.php">Industrial Cleaning</a></li>
                                    <li><a href="/floor-care-services.php">Floor Care Services</a></li>
                                    <li><a href="/window-cleaning-services.php">Window Cleaning Services</a></li>
                                    <li><a href="/school-child-care-cleaning.php">School &amp; Child Care Cleaning</a></li>
                                </ul>
                            </div>
                        </li>
                        <!-- <li><a href="/outreach.php">Outreach</a></li> -->
                        <li class="site-nav-submenu" tabindex="0">
                            <span>About<span class="sr-only">, Sub menu expanded</span></span>
                            <div>
                                <ul>
                                    <li><a href="/about.php">About</a></li>
                                    <li><a href="/testimonials.php">Testimonials</a></li>
                                </ul>
                            </div>
                        </li>
                        <li><a href="/contact.php">Contact</a></li>
                        <li><a href="/quote-request.php">Quote Request</a></li>
                    </ul>
                </nav>
                <div class="site-social site-social--bordered">
                    <ul class="social-accounts">
                        <li><a href="#" target="_blank" data-bb-track-label="Facebook, Header" data-bb-track="button" data-bb-track-on="click" data-bb-track-action="Click" data-bb-track-category="Social Icons"><i class="fab fa-facebook-f"></i><span class="sr-only">Facebook</span></a></li>
                        <li><a href="#" target="_blank" data-bb-track-label="Twitter, Header" data-bb-track="button" data-bb-track-on="click" data-bb-track-action="Click" data-bb-track-category="Social Icons"><i class="fab fa-twitter"></i><span class="sr-only">Twitter</span></a></li>
                        <li><a href="#" target="_blank" data-bb-track-label="Instagram, Header" data-bb-track="button" data-bb-track-on="click" data-bb-track-action="Click" data-bb-track-category="Social Icons"><i class="fab fa-instagram"></i><span class="sr-only">Instagram</span></a></li>
                    </ul>
                </div>  
            </div>            
        </div>
    </div>
</header>


<div class="scrollContact">
    <div class="wrap">
        <a href="tel:+7176459633" class="tel">
            <label><span class="hidden-xs available">CALL</span> <strong>NOW!</strong></label>
            <span><i class="fas fa-phone"></i> <span class="hidden-xs">(717) 645-9633</span></span>
        </a>
    </div>
</div>