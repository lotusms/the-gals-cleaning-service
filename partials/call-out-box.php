<div class="call-out-box row">
    <div class="col-xs-12 col-sm-8">
        <span>Getting a complimentary estimate from one of our staff is very easy and obligation-free.</span>
    </div>
    <div class="col-xs-12 col-sm-4">
        <a href="/quote-request/" class="btn btn-brand-alt">
            <i class="fas fa-broom"></i> <h3>Get A Quote!</h3>
        </a>
    </div>
</div>