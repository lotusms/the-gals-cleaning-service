
<div class="site-footer-desktop site-footer-desktop--show">
    <div class="site-footer-desktop-primary" data-footer-sticky="">
        <div class="site-footer-desktop-primary__container container">
            <ul class="social-accounts">
                <li><a href="https://www.facebook.com/TheGalsCleaningService/" target="_blank" data-bb-track-label="Facebook, Footer" data-bb-track="button" data-bb-track-on="click" data-bb-track-action="Click" data-bb-track-category="Social Icons"><i class="fab fa-facebook-f"></i><span class="sr-only">Facebook</span></a></li>
                <li><a href="https://twitter.com/TheGalsCleaning" target="_blank" data-bb-track-label="Twitter, Footer" data-bb-track="button" data-bb-track-on="click" data-bb-track-action="Click" data-bb-track-category="Social Icons"><i class="fab fa-twitter"></i><span class="sr-only">Twitter</span></a></li>
            </ul>
            <nav class="site-nav">
                <ul class="site-nav-menu">
                    <li><a href="https://www.lotusmarketingsolutions.com" target="_blank">Designed by LOTUS Marketing Solutions</a> </li>
                    <li><a href="/privacy-policy.php">PRIVACY POLICY</a></li>
                    <li><a href="/terms-of-use.php">TERMS OF USE</a></li>
                </ul>
            </nav>
        </div>
    </div>
</div>
